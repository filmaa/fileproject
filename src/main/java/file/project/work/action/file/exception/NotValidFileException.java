package file.project.work.action.file.exception;

public class NotValidFileException extends Exception {

    public NotValidFileException() {
    }

    public NotValidFileException(String message, Throwable cause) {
        super(message, cause);
    }
}
