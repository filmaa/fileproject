package file.project.work.action.file;

import file.project.work.action.file.exception.NotValidFileException;
import file.project.work.action.model.Line;
import file.project.work.action.model.Point;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class LinesFileParser {

    private List<Line> lines = new ArrayList<>();
    private int countOfPoints = 0;

    public List<Line> getLinesFromFile() throws NotValidFileException {
        try {
            FileInputStream fstream = new FileInputStream("file1.txt");

            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                if (!strLine.contains("#")) {
                    if (!strLine.trim().isEmpty()) {
                        Line line = getPointsFromLine(strLine);
                        lines.add(line);
                    }
                }
            }
            return lines;
        } catch (IOException e) {
            throw new NotValidFileException("not valid file1.txt", e);
        }
    }

    private Line getPointsFromLine(final String strLine) {
        Line line = new Line();
        String trim = strLine.trim();
        while (trim.contains("(")) {
            String pointLine = trim.substring(trim.indexOf("(") + 1, trim.indexOf(")"));
            trim = trim.substring(trim.indexOf(")") + 1);
            Point point = createPointFromLine(pointLine);
            countOfPoints++;
            line.getPoints().add(point);
        }
        return line;
    }

    private Point createPointFromLine(String pointLine) {
        Point point = new Point();
        Double x = Double.valueOf(pointLine.substring(0, pointLine.indexOf(";")));
        Double y = Double.valueOf(pointLine.substring(pointLine.indexOf(";") + 1));
        point.setX(x);
        point.setY(y);
        return point;
    }

    public void cleanFile(String filename) {
        try {
            File file = new File(filename);
            PrintWriter printWriter = new PrintWriter(file);
            printWriter.print("");
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getCountOfPoints() {
        return countOfPoints;
    }

    public void printMaxLengthAndMiddleLength(String fileName, List<Line> lines) {
        try {
            File file = new File(fileName);
            Writer output = new BufferedWriter(new FileWriter(file, true));
            int i = 0;
            Double max = 0.0;
            Double sum = 0.0;
            for (Line line : lines) {
                Double lineLength = LinesFileUtil.getLineLength(line);
                sum += lineLength;
                i++;
                if (lineLength > max) {
                    max = sum;
                }
            }

            sum /= i;
            output.write("средняя длина всех линий: " + sum);
            output.flush();
            ((BufferedWriter) output).newLine();
            output.flush();
            output.write("максимальная длина линии: " + max);
            output.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
