package file.project.work.action.file;

import file.project.work.action.model.Line;
import file.project.work.action.model.Point;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;

import javax.swing.*;
import java.util.LinkedList;
import java.util.List;

import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

public class Graphics {

    public static void drawChartWithPoints(LinesFileParser linesFileParser, List<Line> lines) {
        int countOfPoints = linesFileParser.getCountOfPoints();

        double[] xData = new double[countOfPoints];
        double[] yData = new double[countOfPoints];

        int i = 0;

        for (Line line : lines) {
            for (Point point :
                    line.getPoints()) {
                xData[i] = point.getX();
                yData[i] = point.getY();
                i++;
            }
        }

        // Create Chart
        XYChart chart = new XYChartBuilder().width(800).height(600).build();

        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Scatter);
        chart.getStyler().setChartTitleVisible(false);
        chart.getStyler().setLegendPosition(Styler.LegendPosition.InsideSW);
        chart.getStyler().setMarkerSize(16);

        chart.addSeries("points (x,y)", xData, yData);

        JFrame jFrame = new SwingWrapper(chart).displayChart();
        jFrame.setDefaultCloseOperation(HIDE_ON_CLOSE);
    }

    public static void drawChartWithLines(List<Line> lines) {
        // Create Chart
        XYChart chart = new XYChartBuilder().width(800).height(600).build();

        // Customize Chart
        chart.getStyler().setLegendPosition(Styler.LegendPosition.InsideNW);
        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        chart.getStyler().setYAxisLabelAlignment(Styler.TextAlignment.Right);
        chart.getStyler().setYAxisDecimalPattern("$ #,###.##");
        chart.getStyler().setPlotMargin(0);
        chart.getStyler().setPlotContentSize(.95);
        int i = 0;
        for (Line line :
                lines) {
            List<Double> x = new LinkedList<>();
            List<Double> y = new LinkedList<>();
            for (Point point :
                    line.getPoints()) {
                x.add(point.getX());
                y.add(point.getY());
            }
            chart.addSeries(Integer.toString(i), x, y);
            i++;
        }
        JFrame jFrame = new SwingWrapper(chart).displayChart();
        jFrame.setDefaultCloseOperation(HIDE_ON_CLOSE);
    }
}
