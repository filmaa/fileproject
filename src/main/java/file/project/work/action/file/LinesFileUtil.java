package file.project.work.action.file;

import file.project.work.action.model.Line;
import file.project.work.action.model.Point;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URISyntaxException;
import java.util.List;

public class LinesFileUtil {

    public void printLengthOfLineInFile(String fileName, List<Line> lines) {
        try {
            File file = new File(fileName);

            Writer output = new BufferedWriter(new FileWriter(file, true));
            for (Line line :
                    lines) {
                output.write(getLineLength(line).toString());
                output.flush();
                ((BufferedWriter) output).newLine();
                output.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Double getLineLength(Line line) {
        Double sum = 0.;

        List<Point> points = line.getPoints();
        for (int i = 0; i < points.size() - 1; i++) {
            Double x1 = points.get(i).getX();
            Double y1 = points.get(i).getY();
            Double x2 = points.get(i + 1).getX();
            Double y2 = points.get(i + 1).getY();
            sum += Math.pow(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2), 0.5);
        }

        return sum;
    }

}
