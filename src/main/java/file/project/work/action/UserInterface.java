package file.project.work.action;

import file.project.work.action.file.Graphics;
import file.project.work.action.file.LinesFileParser;
import file.project.work.action.file.LinesFileUtil;
import file.project.work.action.file.exception.NotValidFileException;
import file.project.work.action.model.Line;

import java.util.List;
import java.util.Scanner;


public class UserInterface {

    private static LinesFileParser linesFileParser;
    private static List<Line> lines;
    private static LinesFileUtil linesFileUtil;

    public static void startUsing() {
        while (true) {
            printMenu();
            String step = new Scanner(System.in).next();
            switch (step) {
                case "1":
                    runFirstCase();
                    break;
                case "2":
                    runSecondCase();
                    break;
                case "3":
                    runThirdCase();
                    break;
                case "4":
                    System.exit(0);
                default:
                    System.out.println("not correct number");

            }
        }
    }

    private static void printMenu() {
        System.out.println("Choose variant\n"
                + "1. print all points from file1\n"
                + "2. print lines, write length of lines in file2\n"
                + "3. find middle of all lines and line with max length\n"
                + "4. exit");

    }

    private static void runFirstCase() {
        init();
        Graphics.drawChartWithPoints(linesFileParser, lines);
    }

    private static void runSecondCase() {
        init();
        linesFileParser.cleanFile("file2.txt");
        linesFileUtil.printLengthOfLineInFile("file2.txt", lines);
        Graphics.drawChartWithLines(lines);
    }

    private static void runThirdCase() {
        init();
        linesFileParser.cleanFile("file3.txt");
        linesFileParser.printMaxLengthAndMiddleLength("file3.txt", lines);
    }

    private static void init() {
        if (linesFileParser == null || lines == null || linesFileUtil == null) {
            linesFileParser = new LinesFileParser();
            linesFileUtil = new LinesFileUtil();
            try {
                lines = linesFileParser.getLinesFromFile();
            } catch (NotValidFileException e) {
                System.out.println("not valid file1.txt");
            }
        }
    }

}
