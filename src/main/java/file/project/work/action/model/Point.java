package file.project.work.action.model;


public class Point {
    private Double x;
    private Double y;

    public Point() {
    }

    public Point(Double x, Double y) {

        this.x = x;
        this.y = y;
    }

    @Override
    public int hashCode() {
        int result = getX() != null ? getX().hashCode() : 0;
        result = 31 * result + (getY() != null ? getY().hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Point point = (Point) o;

        if (getX() != null ? !getX().equals(point.getX()) : point.getX() != null) {
            return false;
        }
        return getY() != null ? getY().equals(point.getY()) : point.getY() == null;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
