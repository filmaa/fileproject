package file.project.work.action.model;

import java.util.ArrayList;
import java.util.List;

public class Line {

    private List<Point> points;

    public Line() {
    }

    public List<Point> getPoints() {
        if (points == null) {
            points = new ArrayList<>();
        }
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Line line = (Line) o;

        return getPoints() != null ? getPoints().equals(line.getPoints()) : line.getPoints() == null;
    }

    @Override
    public int hashCode() {
        return getPoints() != null ? getPoints().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Line{" +
                "points=" + points +
                '}';
    }
}
